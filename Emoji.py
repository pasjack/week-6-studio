from turtle import *
speed(0)
penup()
right(180)
forward(100)
right(180)
pendown()
def eye():
    circle(60)
    left(90)
    penup()
    forward(30)
    right(90)
    pendown()
    color("blue")
    begin_fill()
    circle(30)
    end_fill()
    left(90)
    penup()
    forward(20)
    right(90)
    pendown()
    color("black")
    begin_fill()
    circle(8)
    end_fill()

eye()

penup()
right(20)
forward(120)
left(20)

pendown()
speed(0)
pencolor("red")
fillcolor("red")
begin_fill()
left(60)
forward(100)

def curve(length):
    for i in range(length):
        left(2)
        forward(1)
    









curve(100)

right(170)
curve(106)
forward(105)
end_fill()
left(70)
pencolor("black")
penup()
forward(170)
pendown()
speed(0)
#snake
def snake():
    fillcolor("green")
    begin_fill()
#body
    setheading(190)
    forward(100)
    circle(8, 180)
    forward(100)
    circle(8, 180)
    setheading(0)
    circle(8, 190)
    forward(80)
    circle(8, 180)
    forward(80)
    circle(8, 180)
    setheading(140)
    forward(50)
    circle(8, 180)
    forward(40)
    penup()
    right(75)
    forward(35)
    end_fill()
    pendown()
#tail
    fillcolor("green")
    begin_fill()
    setheading(350)
    forward(65)
    left(170)
    forward(50)
    end_fill()
#eyes
    penup()
    setheading(90)
    forward(60)
    left(90)
    forward(35)
    pendown()

    fillcolor("black")
    begin_fill()
    setheading(0)
    right(45)
    forward(4)
    circle(1)
    penup()
    left(90)
    forward(5)
    pendown()
    circle(1)
    end_fill()
    penup()
    forward(100)
    end_fill()

snake()
penup()
right(110)
forward(75)
left(90)
pendown()

speed(0)

begin_fill()
forward(20)
left(90)
forward(90)
left(90)
forward(20)
left(90)
forward(90)
end_fill()

penup()
forward(20)
pendown()





def circle():
    begin_fill()
    for i in range(100):
        left(6)
        forward(1)






    end_fill()

circle()

done()

